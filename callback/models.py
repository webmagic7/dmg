# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.

class CallBack(models.Model):
    tel = models.CharField(max_length=250, verbose_name="Телефон")
    pub_date = models.DateField(null=True,auto_now=True, blank=True, verbose_name="Дата публикации")
    def __unicode__(self):
        return self.tel
    class Meta:
        verbose_name_plural = u"Обратные звонки"
        verbose_name_plural = u"Обратный звонок"