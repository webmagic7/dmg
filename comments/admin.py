from django.contrib import admin
from .models import Comments

from django.db import models

class CommentsAdmin(admin.ModelAdmin):
    list_display = ('name', 'text', 'paket',  'published',)
    list_editable = (  'published',  )
    
admin.site.register(Comments, CommentsAdmin)