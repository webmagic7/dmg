# -*- coding: utf-8 -*-
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey

import datetime

class Comments(MPTTModel):
    paket = models.CharField(max_length=250, db_index=True, verbose_name="Пакет")
    item_model = models.CharField(max_length=250, db_index=True, verbose_name="Модель")
    item_id = models.IntegerField(db_index=True, null=True,  verbose_name="id")
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', verbose_name=u"Родитель")
    name = models.CharField(max_length=250, verbose_name="Название")
    text = models.TextField(blank=True, verbose_name="Полное описание")
    published = models.BooleanField(verbose_name="Опубликован")
    date_add = models.DateTimeField(auto_now_add=True, verbose_name="Дата публикации")
    vote = models.DecimalField(max_digits=2, decimal_places=1,db_index=True, null=True, verbose_name="Оценка")
    positive = models.IntegerField(null=True, blank=True, default=0, verbose_name="Позитивных")
    negative = models.IntegerField(null=True, blank=True, default=0, verbose_name="Негативных")

    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Коментарии "
        verbose_name = "Коментарий"

    class MPTTMeta:
        order_insertion_by = ['name']


class Utility(models.Model):
    comment = models.ForeignKey(Comments, blank=True, null=True, verbose_name="Коммент")
    positive = models.BooleanField(verbose_name="Позитивная оценка")
    

    def __unicode__(self):
        return self.comment.name
    class Meta:
        verbose_name_plural = "Оценки"
        verbose_name = "Оценка"

    
