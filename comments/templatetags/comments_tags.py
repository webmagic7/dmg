from django import template
register = template.Library()

@register.inclusion_tag('comments/comments.html')
def comments(paket, item_model, item_id):
    from comments.models import Comments
    nodes = Comments.objects.filter(paket=paket, item_model=item_model,item_id=item_id, published=1)
    return {'nodes':nodes, 'paket':paket, 'item_model':item_model, 'item_id':item_id}