from django.contrib import admin
from content.models import *

from django.db import models





class MenuItemAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'menu', 'slug', 'published', 'ordering')
    list_editable = ('slug', 'published', 'ordering')
    
class ArticleAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'slug', 'category',  'published', 'ordering')
    list_editable = ('slug', 'published', 'ordering')


        


   
admin.site.register(MenuItem, MenuItemAdmin)
admin.site.register(Menu)
admin.site.register(Article, ArticleAdmin)

admin.site.register(Category)

admin.site.register(Snipet)


