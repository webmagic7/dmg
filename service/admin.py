from django.contrib import admin
from .models import *
from content.fields import AdminImageWidget
from django.db import models

class ImagesInline(admin.StackedInline):
    model = Images
    extra = 1
    formfield_overrides = {
        ThumbnailImageField: {'widget': AdminImageWidget},
    }

class PriceInline(admin.StackedInline):
    model = Price
    extra = 1

class FaqInline(admin.StackedInline):
    model = Faq
    extra = 1

class SeoServiceInline(admin.StackedInline):
    model = SeoService
    extra = 1

class SeoCategoryInline(admin.StackedInline):
    model = SeoCategory
    extra = 1
    

class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name',  'slug', 'published', 'ordering')
    list_editable = ('slug', 'published', 'ordering')
    inlines = [PriceInline,SeoCategoryInline,FaqInline]
    formfield_overrides = {
        ThumbnailImageField: {'widget': AdminImageWidget},
    }
    
class ServiceAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'id', 'pic', 'tex', 'short_text', 'h1', 'slug', 'category', 'published', 'ordering')
    list_editable = ('h1','slug', 'published', 'ordering')
    list_filter = ( 'category',)
    search_fields = ['name']
    inlines = [ImagesInline, SeoServiceInline, FaqInline]
    formfield_overrides = {
        ThumbnailImageField: {'widget': AdminImageWidget},
    }

class PriceAdmin(admin.ModelAdmin):
    
    list_display = ('name', 'ed', 'price')
    list_editable = ('price', )
    list_filter = ( 'category',)

class ZakazAdmin(admin.ModelAdmin):
    
    list_display = ('name', 'category', 'service', 'tel', 'text', 'published')
    list_editable = ('published', )

class FaqAdmin(admin.ModelAdmin):
    
    list_display = ('name', 'category', 'service', 'text',  'published', 'ordering')
    list_editable = ('published', 'ordering')
    
        


   
admin.site.register(Images)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Service, ServiceAdmin)
admin.site.register(Price, PriceAdmin)
admin.site.register(Ed)
admin.site.register(Step)
admin.site.register(Prem)
admin.site.register(Why)
admin.site.register(Zakaz, ZakazAdmin)
admin.site.register(Faq, FaqAdmin)
admin.site.register(SeoService)
admin.site.register(SeoCategory)




