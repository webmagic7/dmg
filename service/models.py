# -*- coding: utf-8 -*-
from django.db import models
from content.fields import *

import random 
from django.conf import settings
import datetime
from mptt.models import MPTTModel, TreeForeignKey
from ckeditor.fields import RichTextField


def make_upload_path(instance, filename, prefix = False):
    # Переопределение имени загружаемого файла.
    n1 = random.randint(0,10000)
    n2 = random.randint(0,10000)
    n3 = random.randint(0,10000)
    filename = str(n1)+"_"+str(n2)+"_"+str(n3) + '.jpg'
    return u"%s/%s" % (settings.IMAGE_UPLOAD_DIR, filename)

def make_upload_path2(instance, filename, prefix = False):
    # Переопределение имени загружаемого файла.
    n1 = random.randint(0,10000)
    n2 = random.randint(0,10000)
    n3 = random.randint(0,10000)
    filename = str(n1)+"_"+str(n2)+"_"+str(n3) + '.jpg'
    return u"%s/%s" % ('colors', filename)



class Step(models.Model):
    name = models.CharField(max_length=250, default="", verbose_name="Название")
    template = models.CharField(max_length=250, blank=True, default="", verbose_name="Шаблон")
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Шаги"
        verbose_name = "Шаги"

class Prem(models.Model):
    name = models.CharField(max_length=250, default="", verbose_name="Название")
    template = models.CharField(max_length=250, blank=True, default="", verbose_name="Шаблон")
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Преимущества"
        verbose_name = "Преимущества"

class Why(models.Model):
    name = models.CharField(max_length=250, default="", verbose_name="Название")
    template = models.CharField(max_length=250, blank=True, default="", verbose_name="Шаблон")
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Для чего"
        verbose_name = "Для чего"

class Category(models.Model):
    name = models.CharField(max_length=250, verbose_name="Название")
    image = models.ImageField(upload_to=make_upload_path, default="", blank=True,  verbose_name="Изображение")
    bunner = models.ImageField(upload_to=make_upload_path, default="", blank=True,  verbose_name="Баннер")
    prem = models.ForeignKey(Prem, blank=True, null=True, verbose_name="Преимущесива")
    step = models.ForeignKey(Step, blank=True, null=True, verbose_name="Шаги")
    why = models.ForeignKey(Why, blank=True, null=True, verbose_name="Для чего")
    title = models.CharField(max_length=250, blank=True, verbose_name="Заголовок в браузере")
    metakey = models.CharField(max_length=250, blank=True, verbose_name="Ключевые слова")
    metadesc = models.CharField(max_length=250, blank=True, verbose_name="Мета описание")
    slug = models.CharField(max_length=250, blank=True, verbose_name="Урл")
    short_text = RichTextField(blank=True, verbose_name="Короткое описание")
    full_text = RichTextField(blank=True, verbose_name="Полное описание")
    published = models.BooleanField(verbose_name="Опубликован")
    ordering = models.IntegerField(verbose_name="Порядок сортировки", default=0, blank=True, null=True)
    reit = models.DecimalField(max_digits=2, decimal_places=1, verbose_name="Рейтинг", blank=True, null=True, default=0.0)
    count_votes = models.IntegerField(verbose_name="Количество голосов", blank=True, null=True, default=0)
    comments_count = models.IntegerField(verbose_name="Количество коментариев", default=0, blank=True, null=True)
    def get_faq(self):
        return Faq.objects.filter(category=self)
    def get_url(self):
        return u"/service/%s/" % self.slug 

    def pic(self):
        if self.image:
            return u'<img src="%s" width="70"/>' % self.image.url
        else:
            return '(none)'
    pic.short_description = u'Изображение'
    pic.allow_tags = True
    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = "Категории"
        verbose_name = "Категория"


class SeoCategory(models.Model):
    category = models.ForeignKey(Category, blank=True, verbose_name="Категория")
    name = models.CharField(max_length=250, verbose_name="Название")
    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = "Ключевые слова Категории"
        verbose_name = "Ключевые слова Категория"



class Service(MPTTModel):
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', verbose_name=u"Родительская услуга")
    category = models.ForeignKey(Category, blank=True,null=True, verbose_name="Категория")
    category2 = models.ForeignKey(Category, related_name='cat2', blank=True, null=True, verbose_name="Категория2")
    name = models.CharField(max_length=250, verbose_name="Название")
    h1 = models.CharField(max_length=250, verbose_name="h1")
    price = models.CharField(max_length=250, blank=True, default="", verbose_name="Цена")
    image = models.ImageField(upload_to=make_upload_path, blank=True,  verbose_name="Изображение")
    bunner = models.ImageField(upload_to=make_upload_path, default="", blank=True,  verbose_name="Баннер")
    category = models.ForeignKey(Category, blank=True, verbose_name="Категория")
    title = models.CharField(max_length=250, blank=True, verbose_name="Заголовок в браузере")
    metaKey = models.CharField(max_length=250, blank=True, verbose_name="Ключевые слова")
    metaDesc = models.CharField(max_length=250, blank=True, verbose_name="Описание")
    
    slug = models.CharField(max_length=250, blank=True, verbose_name="Урл")
    short_text = RichTextField(blank=True, verbose_name="Короткое описание")
    full_text = RichTextField(blank=True, verbose_name="Полное описание")
    show = models.BooleanField(verbose_name="Показывать на главной")
    published = models.BooleanField(verbose_name="Опубликован")
    ordering = models.IntegerField(verbose_name="Порядок сортировки", default=0, blank=True, null=True)
    reit = models.DecimalField(max_digits=2, decimal_places=1, verbose_name="Рейтинг", blank=True, null=True, default=0.0)
    count_votes = models.IntegerField(verbose_name="Количество голосов", blank=True, null=True, default=0)
    comments_count = models.IntegerField(verbose_name="Количество коментариев", default=0, blank=True, null=True)
    def get_url(self):
        if self.parent:
            return "/service/%s/%s/%s/" % (self.category.slug, self.parent.slug, self.slug)
        else:
            return "/service/%s/%s/" % (self.category.slug, self.slug)

    def get_faq(self):
        return Faq.objects.filter(service=self)
    def pic(self):
        if self.image:
            return u'<img src="%s" width="70"/>' % self.image.url
        else:
            return '(none)'
    pic.short_description = u'Изображение'
    pic.allow_tags = True

    @property
    def tex(self):
        if self.full_text:
            return 1
        else:
            return 0

    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Услуги"
        verbose_name = "Услуга"
    class MPTTMeta:
        order_insertion_by = ['name']

class SeoService(models.Model):
    service = models.ForeignKey(Service, blank=True, verbose_name="Категория")
    name = models.CharField(max_length=250, verbose_name="Название")
    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = "Ключевые слова услуги"
        verbose_name = "Ключевые слова услуги"

class Images(models.Model):
    service = models.ForeignKey(Service, blank=True, verbose_name="Услуга")
    name = models.CharField(max_length=250, blank=True, default="", verbose_name="Название")
    image = ThumbnailImageField(upload_to=make_upload_path, blank=True, default="",  verbose_name="Изображение")
    published = models.BooleanField(verbose_name="Опубликован")
    ordering = models.IntegerField(verbose_name="Порядок сортировки", default=0, blank=True, null=True)
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Изображения"
        verbose_name = "Изображение"

class Ed(models.Model):
    name = models.CharField(max_length=250, blank=True, default="", verbose_name="Название")
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Единицы измерения"
        verbose_name = "Единица измерения"

class Price(models.Model):
    category = models.ForeignKey(Category, null=True, blank=True, verbose_name="Категория")
    service = models.ManyToManyField(Service, related_name='pri', null=True, blank=True, verbose_name="Услуга")
    name = models.CharField(max_length=250, blank=True, default="", verbose_name="Название")
    ed = models.ForeignKey(Ed, blank=True, null=True, verbose_name="Единица измерения")
    price = models.CharField(max_length=250, blank=True, default="", verbose_name="Цена")
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Цены"
        verbose_name = "Цена"



class Zakaz(models.Model):
    category = models.ForeignKey(Category, blank=True, null=True, verbose_name="Категория")
    service = models.ForeignKey(Service, blank=True, null=True, verbose_name="Услуга")
    name = models.CharField(max_length=250, blank=True, default="", verbose_name="Имя")
    tel = models.CharField(max_length=250, blank=True, default="", verbose_name="Телефон")
    text = models.TextField( blank=True, default="", verbose_name="Сообщение")
    published = models.BooleanField(verbose_name="Обработан", default=False)
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Заказы"
        verbose_name = "Заказ"


class Faq(models.Model):
    category = models.ForeignKey(Category, blank=True, null=True, verbose_name="Категория")
    service = models.ForeignKey(Service, blank=True, null=True, verbose_name="Услуга")
    name = models.CharField(max_length=250, blank=True, default="", verbose_name="Имя")
    text = models.TextField( blank=True, default="", verbose_name="Сообщение")
    published = models.BooleanField(verbose_name="Опубликован")
    ordering = models.IntegerField(verbose_name="Порядок сортировки", default=0, blank=True, null=True)
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Часто задаваемые вопросы"
        verbose_name = "Часто задаваемые вопросы"






    


