# -*- coding: utf-8 -*-
from django.db import models
from django.http import HttpResponse
from django.template import Context, loader
from content.models import Article, Category, Docs
from django.views.decorators.cache import cache_page
from django.core.cache import cache
from django.shortcuts import redirect, render, get_object_or_404
from .models import Category, Service, Price
from works.models import Work, WorkCategory
from .forms import *
from django.core.mail import send_mail, EmailMessage
from turn.models import EmailTurn, SmsTurn
from suds.client import Client
from django.db.models import Q

def services(request):
    cats = Category.objects.select_related().filter(published=1).prefetch_related().order_by('ordering')
    return render(request, 'service/services.html',{'cats':cats})

def lp(request):
    docs = Docs.objects.filter(published=1).order_by('ordering')[:4]
    cat_id = request.POST.get("cat_id")
    if cat_id:
        cat = get_object_or_404(Category, pk=int(cat_id))
    else:
        cat = ""
    return render(request, 'service/lp.html', {'docs':docs, 'cat':cat})
def sitemap(request):
    categorys = Category.objects.filter()
    services = Service.objects.filter(published=1)
    works = Work.objects.filter(published=1)
    workcats = WorkCategory.objects.filter(published=1)
    news =  Article.objects.filter(category=2, published=1).order_by('-pub_date')
    return render(request, 'service/sitemap.html', {"categorys":categorys, 'news':news,
     'services':services, 'works':works, 'workcats':workcats}, content_type="application/xhtml+xml")


@cache_page(60 * 1500)
def home(request):
    cats = Category.objects.select_related().filter(show=1).prefetch_related().order_by('ordering')
    return render(request, 'index.html', {'cats':cats})

def category(request, category):
    category = get_object_or_404(Category, slug=category)
    prices = Price.objects.select_related().filter(category=category).prefetch_related()
    services = Service.objects.filter(category=category, published=1)
    try:
        wk = WorkCategory.objects.get(category=category)
        works = Work.objects.filter(Q(work_category=wk) | Q(category=category) ).distinct()
        works = works.filter(published=1)
    except:
        works = []
    faq = category.get_faq()
    return render(request, 'service/category.html', {'category':category, 
        'works':works, 'services':services, 'faq':faq, 'prices':prices})

def otoplenie(request):
    category = 'sistemy-otopleniya'
    category = get_object_or_404(Category, slug=category)
    faq = category.get_faq()
    prices = Price.objects.select_related().filter(category=category).prefetch_related()
    services = Service.objects.filter(category=category, published=1)
    wk = WorkCategory.objects.get(pk=3)
    works = Work.objects.filter(work_category=wk, published=1)
    docs = Docs.objects.filter(published=1).order_by('ordering')[:4]
    return render(request, 'service/otoplenie.html', {'category':category, 
        'works':works, 'services':services,'faq':faq,  'prices':prices, 'docs':docs})

def project(request):
    category = 'proektirovanie-inzhenernyh-setej'
    category = get_object_or_404(Category, slug=category)
    faq = category.get_faq()
    prices = Price.objects.select_related().filter(category=category).prefetch_related()
    services = Service.objects.filter(pk__in=[62,39, 54, 94, 103, 104], published=1)
    #wk = WorkCategory.objects.get(pk=3)
    works = Work.objects.filter(category=category, published=1)
    #works = []
    docs = Docs.objects.filter(published=1).order_by('ordering')[:4]
    return render(request, 'service/project.html', {'category':category, 
        'works':works, 'services':services, 'faq':faq, 'prices':prices, 'docs':docs})

def kanal(request):
    category = 'sistemy-kanalizacii'
    category = get_object_or_404(Category, slug=category)
    prices = Price.objects.select_related().filter(category=category).prefetch_related()
    services = Service.objects.filter(category=category, published=1)
    wk = WorkCategory.objects.get(pk=1)
    faq = category.get_faq()
    works = Work.objects.filter(work_category=wk, published=1)
    docs = Docs.objects.filter(published=1).order_by('ordering')[:4]
    return render(request, 'service/kanalizac.html', {'category':category, 
        'works':works, 'services':services, 'faq':faq, 'prices':prices, 'docs':docs})

def voda(request):
    category = 'sistemy-vodosnabzheniya'
    category = get_object_or_404(Category, slug=category)
    faq = category.get_faq()
    prices = Price.objects.select_related().filter(category=category).prefetch_related()
    services = Service.objects.filter(category=category, published=1)
    wk = WorkCategory.objects.get(pk=4)
    works = Work.objects.filter(work_category=wk, published=1)
    docs = Docs.objects.filter(published=1).order_by('ordering')[:4]
    return render(request, 'service/voda.html', {'category':category, 
        'works':works, 'services':services, 'faq':faq, 'prices':prices, 'docs':docs})



@cache_page(60 * 1500)
def service(request, category, service):
    service = get_object_or_404(Service, slug=service, published=1)
    cat = service.category
    faq = service.get_faq()
    services = Service.objects.filter(parent=service)
    docs = Docs.objects.filter(published=1).order_by('ordering')[:4]
    works = Work.objects.filter(services=service, published=1)

    
    return render(request, 'service/service.html', {'service':service, 
        'category':cat, 'services':services, 'faq':faq, 'works':works, 'docs':docs})


    
@cache_page(60 * 1500)
def sub_service(request, category, service, sub_service):
    service = get_object_or_404(Service, slug=sub_service)
    faq = service.get_faq()
    if not service.parent:
        return redirect(service.get_url())
    category = get_object_or_404(Category, slug=category)
    docs = Docs.objects.filter(published=1).order_by('ordering')[:4]
    works = Work.objects.filter(services=service, published=1)
    return render(request, 'service/service.html', {'service':service, 'faq':faq, 'works':works, 'category':category, 'docs':docs})


def price(request):
    cats = Category.objects.filter(published=1).order_by('ordering')
    services = Service.objects.select_related().filter(published=1).prefetch_related()
    return render(request, 'price.html',{'services':services, 'cats':cats})




def price(request):
    prices = Price.objects.select_related().filter().prefetch_related()
    cats = Category.objects.select_related().filter().prefetch_related().order_by('ordering')
    return render(request, 'price.html',{'prices':prices, 'cats':cats})


def save_zakaz(request):
    if request.method =='POST':
        if 'ser' in request.POST:
            ser = request.POST['ser']
        else:
            ser = ""
        form = ZakazForm(request.POST)
        if form.is_valid():
            f = form.save()
            t = loader.get_template('mail/zakaz2.html')
            html = t.render(Context({"data":f, 'ser':ser}))
            e = EmailTurn(name=u'Заказ c cайта', to='0938943741@ukr.net', title=u"Заказ с сайта", text=html)
            e.save()

            s = SmsTurn(name=form.cleaned_data['name'],to='+380938943741', text=u'Контактное лицо: %s \nТелефон: %s' %(form.cleaned_data['name'], form.cleaned_data['tel']))
            s.save()
            import threading
            t = threading.Thread(target=send_messages)
            t.start()

    return HttpResponse("ok")


def send_messages():
    e = EmailTurn.objects.filter(published=0)
    s = SmsTurn.objects.filter(published=0)

    for i in e:
        try:
            msg = EmailMessage(i.title, i.text, u"jcdeesign@gmail.com", [u"webmagic@mail.ua",  i.to])
            msg.content_subtype = "html"
            msg.send()
            i.published = 1
            i.save()
        except:
            pass

    for i in s:
        try:
            client = Client('http://turbosms.in.ua/api/wsdl.html')
            r = client.service.Auth(login='osvita',password='1qaz2wsx')
            r = client.service.SendSMS(sender='YmsComUa',destination=i.to, text=i.text)
            i.published = 1
            i.save()
        except:
            pass


    
    

