from django.contrib import admin
from .models import *
from content.fields import AdminImageWidget
from django.db import models

class EmailTurnAdmin(admin.ModelAdmin):
    
    list_display = ('name', 'to', 'published')

class SmsTurnAdmin(admin.ModelAdmin):
    
    list_display = ('name', 'to', 'published')
    
admin.site.register(SmsTurn, SmsTurnAdmin)
admin.site.register(EmailTurn, EmailTurnAdmin)