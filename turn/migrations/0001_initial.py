# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='EmailTurn',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=250, verbose_name=b'\xd0\x9d\xd0\xb0\xd0\xb7\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5')),
                ('to', models.CharField(default=b'', max_length=250, verbose_name=b'\xd0\x90\xd0\xb4\xd1\x80\xd0\xb5\xd1\x81\xd0\xb0\xd1\x82')),
                ('title', models.CharField(default=b'', max_length=250, verbose_name=b'\xd0\x97\xd0\xb0\xd0\xb3\xd0\xbe\xd0\xbb\xd0\xbe\xd0\xb2\xd0\xbe\xd0\xba')),
                ('text', models.TextField(default=b'', max_length=250, verbose_name=b'\xd0\xa2\xd0\xb5\xd0\xba\xd1\x81\xd1\x82')),
                ('published', models.BooleanField(default=False, verbose_name=b'O\xd1\x82\xd0\xbf\xd1\x80\xd0\xb0\xd0\xb2\xd0\xbb\xd0\xb5\xd0\xbd')),
            ],
            options={
                'verbose_name': '\u041e\u0447\u0435\u0440\u0435\u0434\u044c \u043f\u043e\u0447\u0442\u044b',
                'verbose_name_plural': '\u041e\u0447\u0435\u0440\u0435\u0434\u044c \u043f\u043e\u0447\u0442\u044b',
            },
        ),
        migrations.CreateModel(
            name='SmsTurn',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=250, verbose_name=b'\xd0\x9d\xd0\xb0\xd0\xb7\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5')),
                ('to', models.CharField(default=b'', max_length=250, verbose_name=b'\xd0\x90\xd0\xb4\xd1\x80\xd0\xb5\xd1\x81\xd0\xb0\xd1\x82')),
                ('title', models.CharField(default=b'', max_length=250, verbose_name=b'\xd0\x97\xd0\xb0\xd0\xb3\xd0\xbe\xd0\xbb\xd0\xbe\xd0\xb2\xd0\xbe\xd0\xba')),
                ('text', models.TextField(default=b'', max_length=250, verbose_name=b'\xd0\xa2\xd0\xb5\xd0\xba\xd1\x81\xd1\x82')),
                ('published', models.BooleanField(default=False, verbose_name=b'\xd0\x9e\xd1\x82\xd0\xbf\xd1\x80\xd0\xb0\xd0\xb2\xd0\xbb\xd0\xb5\xd0\xbd')),
            ],
            options={
                'verbose_name': '\u041e\u0447\u0435\u0440\u0435\u0434\u044c sms',
                'verbose_name_plural': '\u041e\u0447\u0435\u0440\u0435\u0434\u044c sms',
            },
        ),
    ]
