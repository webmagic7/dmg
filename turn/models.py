# -*- coding: utf-8 -*-
from django.db import models

import random
from django.conf import settings
from content.fields import *
from mptt.models import MPTTModel, TreeForeignKey
import datetime


class EmailTurn(models.Model):
    name = models.CharField(max_length=250, default="",  verbose_name="Название")
    to = models.CharField(max_length=250, default="",  verbose_name="Адресат")
    title = models.CharField(max_length=250, default="",  verbose_name="Заголовок")
    text = models.TextField(max_length=250, default="",  verbose_name="Текст")
    published = models.BooleanField(verbose_name="Oтправлен", default=False)
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Очередь почты"
        verbose_name = "Очередь почты"

class SmsTurn(models.Model):
    name = models.CharField(max_length=250, default="",  verbose_name="Название")
    to = models.CharField(max_length=250, default="",  verbose_name="Адресат")
    title = models.CharField(max_length=250, default="",  verbose_name="Заголовок")
    text = models.TextField(max_length=250, default="",  verbose_name="Текст")
    published = models.BooleanField(verbose_name="Отправлен", default=False)
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Очередь sms"
        verbose_name = "Очередь sms"