from django.contrib import admin
from .models import *
from content.fields import AdminImageWidget
from django.db import models

class ImagesInline(admin.StackedInline):
    model = Images
    extra = 1
    formfield_overrides = {
        ThumbnailImageField: {'widget': AdminImageWidget},
    }

class ParInline(admin.StackedInline):
    model = Par

class WorkCategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'category', 'slug', 'published', 'ordering')
    list_editable = ('category','slug', 'published', 'ordering')
    
    formfield_overrides = {
        ThumbnailImageField: {'widget': AdminImageWidget},
    }
    
class WorkAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'pic',  'slug', 'published', 'ordering')
    list_editable = ('slug',  'published', 'ordering')
    filter_horizontal = ("category","services")
    search_fields = ["name",]
    inlines = [ImagesInline,ParInline]
    formfield_overrides = {
        ThumbnailImageField: {'widget': AdminImageWidget},
    }
        


   
admin.site.register(Images)

admin.site.register(Work, WorkAdmin)
admin.site.register(WorkCategory, WorkCategoryAdmin)




