# -*- coding: utf-8 -*-
from django.db import models
from .fields import *

from ckeditor.fields import RichTextField
import random 
from django.conf import settings
from service.models import Category, Service


def make_upload_path(instance, filename, prefix = False):
    # Переопределение имени загружаемого файла.
    n1 = random.randint(0,10000)
    n2 = random.randint(0,10000)
    n3 = random.randint(0,10000)
    filename = str(n1)+"_"+str(n2)+"_"+str(n3) + '.jpg'
    return u"%s/%s" % (settings.IMAGE_UPLOAD_DIR, filename)


    
class WorkCategory(models.Model):
    name = models.CharField(max_length=250, default="", verbose_name="Название")
    category = models.ForeignKey(Category, blank=True, null=True, verbose_name="Категория")
    adres = models.CharField(max_length=250, blank=True, default="", verbose_name="Адрес")
    image = ThumbnailImageField(upload_to=make_upload_path, blank=True,  verbose_name="Изображение")
    title = models.CharField(max_length=250, blank=True, verbose_name="Заголовок в браузере")
    metaKey = models.CharField(max_length=250, blank=True, verbose_name="Ключевые слова")
    metaDesc = models.CharField(max_length=250, blank=True, verbose_name="Описание")
    slug = models.CharField(max_length=250, db_index=True, blank=True, verbose_name="Урл")
    show = models.BooleanField(verbose_name="Показывать на главной")
    published = models.BooleanField(verbose_name="Опубликован")
    ordering = models.IntegerField(verbose_name="Порядок сортировки", default=0, blank=True, null=True)
    def get_url(self):
        return "/works/%s/" % self.slug

    def pic(self):
        if self.image:
            return u'<img src="%s" width="70"/>' % self.image.url
        else:
            return '(none)'
    pic.short_description = u'Изображение'
    pic.allow_tags = True

    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Категории"
        verbose_name = "Категории"




class Work(models.Model):
    name = models.CharField(max_length=250, default="", verbose_name="Название")
    work_category = models.ForeignKey(WorkCategory, blank=True, null=True, verbose_name="Категория")
    adres = models.CharField(max_length=250, blank=True, default="", verbose_name="Адрес")
    image = ThumbnailImageField(upload_to=make_upload_path, blank=True,  verbose_name="Изображение")
    category = models.ManyToManyField(Category, blank=True, null=True, verbose_name="Category")
    services = models.ManyToManyField(Service, blank=True, null=True, verbose_name="Service")
    title = models.CharField(max_length=250, blank=True, verbose_name="Заголовок в браузере")
    metaKey = models.CharField(max_length=250, blank=True, verbose_name="Ключевые слова")
    metaDesc = models.CharField(max_length=250, blank=True, verbose_name="Описание")
    slug = models.CharField(max_length=250, db_index=True, blank=True, verbose_name="Урл")
    short_text = RichTextField(blank=True, verbose_name="Короткое описание")
    full_text = RichTextField(blank=True, verbose_name="Полное описание")
    show = models.BooleanField(verbose_name="Показывать на главной")
    published = models.BooleanField(verbose_name="Опубликован")
    ordering = models.IntegerField(verbose_name="Порядок сортировки", default=0, blank=True, null=True)
    def get_url(self):
        return "/works/%s/%s/" % (self.work_category.slug, self.slug)


    def pic(self):
        if self.image:
            return u'<img src="%s" width="70"/>' % self.image.url
        else:
            return '(none)'
    pic.short_description = u'Изображение'
    pic.allow_tags = True

    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Работы"
        verbose_name = "Работы"

class Par(models.Model):
    work = models.ForeignKey(Work, blank=True, verbose_name="Услуга")
    name = models.TextField(max_length=250, blank=True, default="", verbose_name="Название параметра")
    val = models.TextField(max_length=250, blank=True, default="", verbose_name="Значение параметра")
    
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Параметры объекта"
        verbose_name = "Параметр объекта"

class Images(models.Model):
    work = models.ForeignKey(Work, blank=True, verbose_name="Услуга")
    name = models.CharField(max_length=250, blank=True, default="", verbose_name="Название")
    image = ThumbnailImageField(upload_to=make_upload_path, blank=True, default="",  verbose_name="Изображение")
    published = models.BooleanField(verbose_name="Опубликован")
    ordering = models.IntegerField(verbose_name="Порядок сортировки", default=0, blank=True, null=True)
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Изображения"
        verbose_name = "Изображение"



    


