from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    # Examples:
     url(r'^$', 'works.views.works', name='works'),
     url(r'^(?P<slug>[-a-z0-9_]+)/$', 'works.views.category_works', name='category_works'),
     url(r'^(?P<category>[-a-z0-9_]+)/(?P<slug>[-a-z0-9_]+)/$', 'works.views.work', name='work'),
     

    
  
)
