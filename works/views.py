# -*- coding: utf-8 -*-
from django.db import models
from django.http import HttpResponse
from django.template import Context, loader
from django.views.decorators.cache import cache_page
from django.core.cache import cache
from django.shortcuts import redirect, render, get_object_or_404
from .models import *
from service.models import Category

def works(request):
    works = WorkCategory.objects.filter(published=1).order_by('ordering')
    return render(request, 'works/works.html', {'works':works})

def work(request, category, slug):
    category = WorkCategory.objects.get(slug=category)
    work = Work.objects.select_related().get(slug=slug)
    images = Images.objects.filter(work=work)
    params = Par.objects.filter(work=work)
    return render(request, 'works/work.html', {'work':work,'params':params, 'category':category, 'images':images})

def category_works(request, slug):
    cat = WorkCategory.objects.get(slug=slug)
    works = Work.objects.filter(published=1, work_category=cat).order_by('ordering')
    
    return render(request, 'works/category.html', {'works':works, 'cat':cat})

def av(request):
    cat = WorkCategory.objects.get(slug='tehnicheskoe-obsluzhivanie-inzhenernyh-setej')
    works = Work.objects.filter(published=1, work_category=cat).order_by('ordering')
    cat = WorkCategory.objects.get(slug='avarijnaya-sluzhba')
    
    return render(request, 'works/category.html', {'works':works, 'cat':cat})